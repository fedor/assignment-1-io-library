section .text
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, rdi
    .loop:
        cmp byte[rdi], 0
        je .return
        inc rdi
        jmp .loop
    .return:
        sub rdi, rax
        mov rax, rdi
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov rdi, rsp
    sub rsp, 24
    dec rdi
    mov byte [rdi], 0
    mov r10, 10
    .loop:
        xor rdx, rdx
        div r10
        add dl, '0'
        dec rdi
        mov byte [rdi], dl
        test rax, rax
        jnz .loop
    call print_string
    add rsp, 24
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .pos 
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .pos:
        sub rsp, 8
        call print_uint
        add rsp, 8
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	mov dl, [rdi]
	cmp dl, [rsi]
	jnz .return
	inc rdi
	inc rsi
	test dl, dl
	jnz string_equals
	inc rax
    .return:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rdi, rdi              
    xor rax, rax
    mov rsi, rsp                   
    mov rdx, 1            
    syscall
    pop rax                 
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push r12
    push r13
	push r14
	xor r12, r12
	mov r13, rsi
	mov r14, rdi
	.whitespace: 
        call read_char
        cmp al, `\n`
        je .whitespace
        cmp al, ' '
        je .whitespace
        cmp al, `\t`
        je .whitespace
        test al,al
        jz .return
        test r13,r13
        je .err
        dec  r13         
	.read:
        cmp r12,r13     
        je .err
        mov byte[r14+r12], al
        inc r12
        call read_char
        cmp al, `\n`
        je .return
        cmp al, ' '
        je .return
        cmp al, `\t`
        je .return
        test al,al
        jz .return
        jmp .read
    .err:
        xor rax,rax
        pop r14
        pop r13     
        pop r12
        ret
    .return:
        mov byte[r12+r14], 0
        mov rax,r14
        mov rdx,r12     
        pop r14
        pop r13
        pop r12
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rax, rax
    mov r10, 10
    xor r11, r11
    .next:
        mov sil, byte[rdi + r11]
        cmp sil, 48
        jb .return
        cmp sil, 57
        ja .return
        sub sil, 48
        mul r10
        add rax, rsi
        inc r11
        jmp .next
    .return:
        mov rdx, r11
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], 45
    jne .pos
    inc rdi
    push rdi
    call parse_uint
    pop rdi
    neg rax
    inc rdx
    ret
    .pos:
        push rdi
        call parse_uint
        pop rdi
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    .loop:
        test rdx, rdx
        jz .overflow
        mov r8b, [rdi + rcx]
        mov [rsi + rcx], r8b
        test r8b, r8b
        jz .return
        inc rcx
        dec rdx
        jmp .loop
    .overflow:
        xor rcx, rcx
    .return:
        mov rax, rcx
        ret
